function [Result,Result2] = GLI_final(seismic,time_series,ti,wavelet,I,type,nI)
%the program will run 100.000 iterations and it will test the value for
%convergence... I thought it would be better this way because we avoid
%infinite loops 

%please but everything as a column vector 
%seismic = observed data
%time_series = time array
%ti = time boundary location of each boundary
%wavelet = ricker wavelet 
%I = guess of impedance profile (one value for each lithology block)
%type = 1 for 
%following instructions below



%% sismica estimada
dt = 0.004;
time_p = floor(ti/dt);
seismic2 = zeros(size(time_series,1),1);

for j = 1:size(I,1) - 1
    rc_estimated(j,1) = (I(j+1) - I(j))/(I(j+1) + I(j));
    seismic2(time_p(j),1) = rc_estimated(j,1);
end

seismic2 = conv(seismic2,wavelet,'same');
%size(seismic2)
plot(seismic2,'-k','linewidth',2)



return
%%
%impedance
%nI = [15,18,25,28,29,30,31,32,36,40,41,43,44,46,47,57,61];
%nI = nI';
nB = [0];

%% defining the number of layers
nL = size(I,1);

%Creating sensitivity matrix

S_i = zeros(size(time_series,1),1);
S_b = zeros(size(time_series,1),1);
%S_g = zeros(size(time_series,1),1);
%S_w = zeros(size(time_series,1),1);
%S_s = zeros(size(time_series,1),1);


for jj=1:600000
    
    switch type
        case 2
            
            %%creating the impedance matrix for the sensibility matrix
            for u = 1:nL
                
                dFdI = d_impedance(seismic2,I,time_p,wavelet,u);
                S_i(:,u) = dFdI;
                
            end
            if nI ~= 0
                for j = 1:size(nI,1)
                    S_i(:,nI(j)) = 0;
                end
            end
            
            S_b = zeros(size(time_series,1),size(ti,1));

        case 1
                 
            % building the time boundary location matrix
            if size(nB,1) == size(ti,1)
                for i = 1:size(nB,1)
                    S_b(:,nB(i)) = 0;
                end
            else
                for v = 1:size(ti,1)
                    dFdt = d_boundary(time_series,ti(v),wavelet,I(2),I(1));
                    S_b(:,v)   = dFdt;
                end
            end
            
            S_i = zeros(size(time_series,1),size(I,1));
    end
    



%% computing the error vector and the sensibility matrix S

dif = seismic - seismic2;
S = [S_i,S_b];
%dif = dif(1:end-1,:);

%% solving the inversion problem
R = (S'*dif)\(S'*S);

%plot(S*R')
%hold on

%% updating the model
% time boundary location (must be the first parameter inverted for)
for i = (size(S_i,2)+1):(size(S_b,2)+size(S_i,2))
    if R(i) > 0 && ti(i - size(S_i,2)) ~= ti(end)
        ti(i - size(S_i,2)) = ti(i - size(S_i,2)) + 0.004;  %1ms is the step for each iteration
    end
    
    if R(i) < 0 
        ti(i - size(S_i,2)) = ti(i- size(S_i,2)) - 0.004;
        if ti(i - size(S_i,2))< 0.006
            ti(i - size(S_i,2)) = 0.004;
        end
    end
end

%impedances.
for i = 1:size(I,1)
    I_new(i,1) = I(i,1) + 100*R(i);

end


%reflectivity

for j = 1:size(I,1) - 1
    rc(j,1) = (I_new(j+1) - I_new(j))/(I_new(j+1) + I_new(j));
end

a = lscov(seismic2,seismic);



%test = sum((seismic - seismic2).^(2));

if a > 0.9999999 && a < 1.00000001
%if test < 0.000002
    fprintf('\n The value of Impedances reached: %f \n',I)
    
    for j = 1:size(I,1) - 1
    rc(j,1) = (I(j+1) - I(j))/(I(j+1) + I(j));
    end
    fprintf('\n number of iterations: %.0f \n',jj)
    Result = I;
    Result2 = seismic2;
    
    figure
    plot(seismic)
    hold on
    plot(seismic2,'-r')
    title('After inversion','fontsize',20)
    set(gca,'fontsize',18)
    grid on
    ylabel('time series (s)')
    xlabel('amplitude')
    legend('Original trace','After inversion')
    break
    
end    
I = I_new;   
pt = floor(ti/dt);
seismic2 = zeros(size(time_series,1),1);

for i = 1:size(rc,1)
    seismic2(pt(i),1) = rc(i);
end

seismic2 = conv(seismic2,wavelet,'same');


end
end


function new_trace = d_impedance(seismic,I,time_p,wavelet,impedance)


n_last = size(I,1);
samples = size(seismic,1);


new_trace = zeros(samples,1);
if impedance == 1 
    RC = -2*I(impedance+1)/( I(impedance+1) + I(impedance) ).^(2);
    new_trace(time_p(impedance,1),1) = RC;
    new_trace = conv(new_trace,wavelet,'same');
end

if impedance ~= 1 && impedance ~= n_last
    RC1 =  2*I(impedance-1)/( I(impedance) + I(impedance-1)).^(2);
    RC2 = -2*I(impedance+1)/( I(impedance+1) + I(impedance)).^(2);
    new_trace(time_p(impedance-1,1),1) = RC1;
    new_trace(time_p(impedance,1),1)   = RC2;
    new_trace = conv(new_trace,wavelet,'same');
end

if impedance == n_last
    RC = 2*I(impedance-1)/( I(impedance-1) + I(impedance) ).^(2);
    new_trace(time_p(impedance-1,1),1) = RC;
    new_trace = conv(new_trace,wavelet,'same');
end

end
    
function Md_bn = d_boundary(time_series,ti,wavelet,I2,I1)

%all the vectors are column vectors
%the derivative operator

a = (I2 - I1)/(I2 + I1);

OP = operator(time_series,ti);



Md_b = a.*conv(-OP,wavelet,'same');
Md_bn = Md_b./max(Md_b);





end






  




