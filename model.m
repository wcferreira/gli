function [seismic1,time_series,ti,wavelet,I_estimated,I_real] = model()
%medium 1 
vp1   = 1800;
rho1  = 2.1;

i1 = vp1*rho1;

%medium 2
vp2   = 2000;
rho2  = 2.3;

i2 = vp2*rho2;
i3 = i1; %+ (10/100)*i1;
i4 = i2 + round((4/100)*i2);


%reflection coefficient
rc1 = (i2 - i1)/(i2+i1);
rc2 = (i3 - i2)/(i3+i2);
rc3 = (i4 - i3)/(i4+i3);


%seismic true
dt = 0.001;
time_series = (dt:dt:1);
time_series = time_series';

seismic1        = zeros(1000,1);
seismic1(200,1) = rc1;
seismic1(220,1) = rc2;
seismic1(600,1) = rc3;



wavelet  = ricker_wavelet(30,0.04,dt);

seismic1 = conv(seismic1,wavelet,'same');
plot(seismic1,time_series)
axis ij


%  %  %  %

i1_new = i1; % -i1*(2/100) + i1; 
i2_new = i2*(6/100) + i2;
i3_new = i1*(2/100) + i1;
i4_new = i3_new + 100;


rc1_new = (i2_new - i1_new)/(i2_new + i1_new);
rc2_new = (i3_new - i2_new)/(i3_new + i2_new);
rc3_new = (i4_new - i3_new)/(i4_new + i3_new);


seismic2        = zeros(1000,1);
seismic2(200,1) = rc1_new;
seismic2(220,1) = rc2_new;
seismic2(600,1) = rc3_new;



seismic2 = conv(seismic2,wavelet,'same');
hold on
grid on
plot(seismic2,time_series,'-r')
set(gca,'fontsize',18)
legend('Original trace','Estimated trace')
ylabel('time series (s) ')
xlabel('amplitude')

%other values

ti = [0.20; 0.22; 0.6];
time_p = ti/dt;
I_estimated = [i1_new; i2_new; i3_new; i4_new];
I_real = [i1; i2; i3; i4];

end


function amplitude = ricker_wavelet(Fc,time,dt)


 %% Time specifications:
   %Fs = 800;                      % samples per second
   %dt = 0.001;                     % seconds per sample
                 
   t = (-(time):dt:time)';
   N = size(t,1);
   
   %% Sine wave:
   p = Fc.*Fc.*t.*t;
   amplitude = (1-2*pi*pi.*p).*exp(-pi*pi.*p);

   %% Fourier Transform:
   %X = fftshift(fft(amplitude));
   %% Frequency specifications:
   %dF = Fs/N;                      % hertz
   %f = -Fs/2:dF:Fs/2-dF;           % hertz
   
   %%Calculating the area under the wavelet
   m = size(amplitude,1);
   coeff = 2*ones(1,m-2);
   coeff = [1, coeff, 1];
   int = (0.5)*sum(amplitude'.*coeff); %trapezoid method
   
   %% Plot the spectrum:
   figure;
   plot(t,amplitude)
   xlabel('time (s)','Fontsize',18);
   title(['Area under the curve: ',num2str(int)],'Fontsize',20);
   

   %figure;
   %plot(f,abs(X)/N);
   %xlabel('Frequency (in hertz)');
   %title('Magnitude Response');
   


end




