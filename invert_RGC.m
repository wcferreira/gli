function [Rpo,G,C] = invert_RGC(seismic,angles,to,tf,dt)


lambda = 2/100;
pi = to/dt;
pf = tf/dt;

for i=pi:pf
    
    R = (seismic(i,:))';
    theta = angles(i,:);
    
    
    
    s = ( (sin(theta)).^(2) )' ;
    t = ( (tan(theta)).^(2) )' .* s ;
    
    M = [ones(size(seismic,2),1),s,t];
    
    P = (M'*M)\(M'*R);
    
    Rpo(i-pi+1) = P(1);
    G(i-pi+1)   = P(2);
    C(i-pi+1)   = P(3);
    
end

