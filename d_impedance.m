function new_trace = d_impedance(seismic,I,time_p,wavelet,impedance)


n_last = size(I,1);
samples = size(seismic,1);
%dI = 0.001*I;
%for i = 1:size(seismic,1)-1 
 %   d_st(i) = ((seismic((i+1)) - seismic((i)))/dI);
%end
%d_st = d_st';

%analitical solution
%ddI = I2 - I1;

%ds_dI = ((-1)^(u)).*2.*seismic./(2*I1 + ddI);
%pp_f = (t + 0.1)/0.001;
%pp_i = (t - 0.1)/0.001;

%ds_dI(1:pp_i,1) = 0;
%ds_dI(pp_f:end,1) = 0;

new_trace = zeros(samples,1);
if impedance == 1 
    RC = -2*I(impedance+1)/( I(impedance+1) + I(impedance) ).^(2);
    new_trace(time_p(impedance,1),1) = RC;
    new_trace = conv(new_trace,wavelet,'same');
end

if impedance ~= 1 && impedance ~= n_last
    RC1 =  2*I(impedance-1)/( I(impedance) + I(impedance-1)).^(2);
    RC2 = -2*I(impedance+1)/( I(impedance+1) + I(impedance)).^(2);
    new_trace(time_p(impedance-1,1),1) = RC1;
    new_trace(time_p(impedance,1),1)   = RC2;
    new_trace = conv(new_trace,wavelet,'same');
end

if impedance == n_last
    RC = 2*I(impedance-1)/( I(impedance-1) + I(impedance) ).^(2);
    new_trace(time_p(impedance-1,1),1) = RC;
    new_trace = conv(new_trace,wavelet,'same');
end

    






%finite differences - elita derivations
%h = 0.1*I;
%dI = I_full(2) - I_full(1);
%rc = dI/((2*I + h) + dI);

%I1 = I_full(1) + h;
%rc = (I_full(2) - I1)/(I_full(2) + I1);

%seismic_dt = zeros(1000,1);
%seismic_dt(500,1) = rc;
%seismic_dt = conv(seismic_dt,wavelet,'same');

%for i = 1:size(seismic,1)
 %   ds_dI(i,1) = (seismic_dt(i) - seismic(i))/h;
%end

