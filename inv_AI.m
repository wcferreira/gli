function AI = inv_AI(NI,ao)


%Initial acoustic impedance
AI = zeros(size(NI,1),1);
AI(1) = ao;


for i = 1:size(NI,1)

    AI(i+1) = (1 + NI(i))./(1 - NI(i))*AI(i);
    
end
