function op = operator(time_series,ti)

%ti = location (in time) of a boundary

n = 120;
t = time_series - ti;
op = ((-2*n^(3))/pi).*t.*exp(-(n^2).*(t.^(2)));

%dI = 0.01;
%for i = 1:size(wavelet,1)-1 
 %   d_st(i) = ((wavelet((i+1)) - wavelet((i)))/dI);
%end
%dt = 0.001;
%time = 0.5;
%f = 30;
%t = (-(time):dt:time)';
%A = -pi*pi*f*f;

%d_ricker = -6.*A.*t.*exp(-A.*t.*t) + 4.*A.*A.*t.*t.*t.*exp(-A.*t.*t);
%plot(t,d_ricker,'-r');

end
