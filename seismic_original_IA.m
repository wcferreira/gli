function [seismic,time_series] = seismic_original_IA(IA,Time,dt,Fq)

time_series = dt:dt:(Time(end) + 50*dt);
time_series = time_series';
time_p = floor(Time/dt);
seismic = zeros(size(time_series,1),1);


for i = 1:size(IA,1)-1
    rc(i,1) = (IA(i+1) - IA(i))/(IA(i+1) + IA(i));
    seismic(time_p(i),1) = rc(i,1);
end

Ricker = ricker_wavelet(Fq,0.04,dt);
seismic = conv(seismic,Ricker,'same');

end
