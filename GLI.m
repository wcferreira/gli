function [S] = GLI(seismic,time_series,ti,wavelet,I,seismic2,rc2)


%seismic2 = (1/0.6651)*seismic2;
wavelet = wavelet;
%Creating sensitivity matrix
K = 2;
S = zeros(size(time_series,1),7);

for jj=1:30

    
for k = 1:size(I,1)
    
    dFdI = d_impedance(seismic2,I(k),I,wavelet);
    S(:,k) = dFdI;
    %S(:,k) = zeros(size(S,1),1);
end

S(:,1) = 0;

for k = 1:size(ti,1)
    
  dFdt = d_boundary(time_series,ti(k),wavelet,I(2),I(1));
 % dFdI = d_impedance(seismic,I(k));
  %dFdt = dFdt/max(dFdt);
  
  %S(:,3*k)   = dFdt;
  S(:,3*k) = zeros(size(S,1),1);
  
end


S(:,4) = zeros(size(S,1),1);
S(:,5) = zeros(size(S,1),1);
S(:,6) = zeros(size(S,1),1);
S(:,7) = zeros(size(S,1),1);

dFdK = d_factor(seismic,K);
%S(:,7) = dFdK(1:end-1,1);


dif = seismic - seismic2;
%dif = dif(1:end-1,:);



R = (S'*dif)\(S'*S);
plot(S*R')
hold on


%new model
 
%ti = abs( ti + R(3)*0.001 );



if R(3) > 0
    ti = ti + 0.001;
end

if R(3) < 0
    ti = ti - 0.001;
end


I2 = I(2) + 100*R(2);
I1 = I(1) + 100*R(1);
I = [I1; I2]
rc = (I2 - I1)/(I2 + I1);


pt = floor(ti/0.001);
seismic2 = zeros(1000,1);
seismic2(pt,1) = rc;
seismic2 = conv(seismic2,wavelet,'same');
%K = K + R(7);
%seismic2 = seismic2;

end

 
  




