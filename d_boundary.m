function Md_bn = d_boundary(time_series,ti,wavelet,I2,I1)

%all the vectors are column vectors
%the derivative operator

a = (I2 - I1)/(I2 + I1);

OP = operator(time_series,ti);



Md_b = a.*conv(-OP,wavelet,'same');
Md_bn = Md_b./max(Md_b);





end



