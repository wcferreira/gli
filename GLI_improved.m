function [seismic2_c,seismic_c] = GLI_improved(seismic,time_series,ti,wavelet,I,seismic2,K)
%the program will run 100.000 iterations and it will test the value for
%convergence... I thought it would be better this way because we avoid
%infinite loops 


%please but everything as a column vector 
%seismic = observed data
%time_series = time array
%ti = time boundary location of each boundary
%wavelet = ricker wavelet 
%I = guess of impedance profile (one value for each lithology block)
%seismic2 = guess seismic trace  using the guess impedance profile
%dBB = gradient of impedance for each block (in our program we are not
%using it, considering all zeros, so just enter with zeros for each layer)
%(**we may consider to just remove this part from the code anyway)
%K = scale factor.. if it will be fixed just input anynumber and see the
%following instructions below


%do we want to fix any parameter in the matrix? Follow the next steps for
%each parameters that will be inverted. Give the number of the fixed
%parameter for each case.


%time boundary location
nB = [1];

%wavelet parameters (central frequency for ricker wavelet)
nW = [1];

%scale factor
nS = [1]; % ns = 1 means you want to fix the parameter, nS = 0 means you want to invert for it. You need to give its guess



%% defining the number of layers
nL = size(I,1);

%pre-allocating one column of each sensitivity matrix
S_i = zeros(size(time_series,1),1);
S_b = zeros(size(time_series,1),1);
S_g = zeros(size(time_series,1),1);
S_w = zeros(size(time_series,1),1);
S_s = zeros(size(time_series,1),1);


%preparing the data that will be inverted on parts
n_interface = size(ti,1);
ntime = size(time_series,1);
seismic_c = cell(n_interface,1);

pp = floor((ti + 0.05)/0.001); %positions of boundary.. 1 is starting point
pp = [1; pp];

for uu = 1:n_interface
    
    seismic_c{uu,1} = zeros(ntime,1);
    seismic2_c{uu,1} = zeros(ntime,1);
    
   
    
end


%index layer
i = 1;

for vv=1:n_interface
    
for jj=1:5000
   
    
seismic_c{i,1}(pp(i):pp(i+1),1) = seismic(pp(i):pp(i+1),1);    
seismic2_c{i,1}(pp(i):pp(i+1),1) = seismic2(pp(i):pp(i+1),1);     


    
%%creating the impedance matrix for the sensibility matrix      
S_i(:,1) = 0;

%selecting the correct impedance from guess
I1 = I(i,1);
I2 = I(i+1,1);

dFdI = d_impedance(seismic2_c{i,1},I1,I2,(i+1));
S_i(:,2) = dFdI;
    
    

%% building the time boundary location matrix 
if nB == 1
    S_b(:,1) = 0;
    
else
    dFdt = d_boundary(time_series,ti(i),wavelet,I(i+1,1),I(i));
    S_b(:,1)   = dFdt;
   
end


%% building the impedance gradient matrix
%if size(nG,1) == size(dBB,1)
 %   for i = 1:size(nG,1)
  %      S_g(:,nG(i)) = 0;
   % end
%else
 %   S_g(:,4) = zeros(size(S,1),1);
  %  S_g(:,5) = zeros(size(S,1),1);
%end

%% building the wavelet parameters matrix
S_w(:,1) = zeros(size(time_series,1),1);

%% building the scale factor matrix
% K is the guess for the scale factor
if nS == 1
    S_s(:,1) = zeros(size(time_series,1),1);
else
    dFdK = d_factor(seismic,K);
end


%% computing the error vector and the sensibility matrix S

dif = seismic_c{i,1} - seismic2_c{i,1};
S = [S_i, S_b, S_w,S_s];

%% solving the inversion problem
R = (S'*dif)\(S'*S);


%% updating the model
% time boundary location (must be the first parameter inverted for)

if R(3) > 0
    ti = ti + 0.001;  %1ms is the step for each iteration
end

if R(3) < 0
    ti = ti - 0.001;
end


%% updating the impedances.
I_new(i+1,1) = I(i+1,1) + 100*R(2);
rc = (I_new(i+1,1) - I(i,1))/(I_new(i+1,1) + I(i,1));

%correcoes
a = lscov(seismic2_c{i,1},seismic_c{i,1}); %evaluating the error value 

if a > 0.999 && a < 1.001
    fprintf('\n The value of Impedances: %f \n',I)
    i = i + 1;
    seismic2_c{i,1} = zeros(ntime,1);
    seismic_c{i,1} = zeros(ntime,1);
    break           
end

I(i+1,1) = I_new(i+1,1);   
pt = floor(ti(i)/0.001);
seismic2_new = zeros(1000,1);

seismic2_new(pt,1) = rc;
seismic2_new = conv(seismic2_new,wavelet,'same');
seismic2 = seismic2_new;


end
end


 



