function dFdK = d_factor(seismic,K)

dFdK = seismic./K;

end