function parameters = user_guess(nlayers, dt, tdepth, tthickness, vp, rho)

%--- Defining model parameters

if nlayers==0 || nlayers==1
    disp('Bad choice! You need at least 2 layers!');
else
    dt = input ('The sampling rate in miliseconds is =  ');
    tdepth = input ('The total time depth of the model is =  ');
    VP = zeros(floor(tdepth/dt),1);
    RHO = zeros (floor(tdepth/dt),1);
    lt=1;
    
    size(VP)
    size(RHO)
    
    for i=1:nlayers
        fprintf('Enter with the parameters for layer %.1f:', i);
        tthickness(i) = input('time thickness =  ');
        vp = input ('velocity =  ');
        rho = input ('density =  ');
        
        thickness = sum(tthickness);
        pp = floor(thickness/dt);
        
        %for j=lt:pp % creates a velocity and density profile
            VP(lt:pp) = vp;
            RHO(lt:pp) = rho;
        %end
        lt=floor((tthickness+dt)/dt);
    end
end


VP
RHO
%--- Calculating time profile

t=zeros(tdepth/dt,1);

for p=0:dt:tdepth
    t(p)=tdepth/dt;
end


%--- Calculating impedance profile

AI=zeros(size(VP),1);

for m=0:(size(VP)-1)
    AI(m)=VP(m).*RHO(m);
end




%--- Calculating reflectivity profile

rc=zeros(size(AI),1);

for k=0:(size(AI)-1)
    rc(k)=(AI(k+1)-AI(k))./(AI(k+1)+AI(k));
end


%--- Calculating time position profile

tp=zeros(size(rc),1);

for n=0:(size(rc)-1)
    if rc(n)==0
        tp(n)=0;
    else
        tp(n)=n*dt;
    end
end


%--- Calculating seismic trace

strace = zeros(size(rc),1);
wavelet = ricker_wavelet(30, 0.04, dt);
strace = conv(rc,wavelet,'same');


%--- Display profiles

   figure;
   title(['Guess Model: ',num2str(int)],'Fontsize',20);
   
   subplot(1,2,1),plot(t,AI)
   xlabel('Acoustic Impedance','Fontsize',18);
   ylabel('Time (ms)','Fontsize',18);
   
   subplot(1,2,2),plot(t,rc)
   xlabel('Reflectivity','Fontsize',18);
   ylabel('Time (ms)','Fontsize',18);
       
   subplot(1,2,3),plot(t,strace)
   xlabel('Seismic Trace','Fontsize',18);
   ylabel('Time (ms)','Fontsize',18);
   
end









