function matrix_coef = cor(data1,data2)

[m,n] = size(data1);


for i = 1:n
  R = corrcoef(data1(:,i),data2(:,i))
  matrix_coef(1,i) = R(2,1);
end

